#!/usr/bin/perl
#+##############################################################################
#                                                                              #
# File: mbsd2dump                                                              #
#                                                                              #
# Description: generate dump files from MBSD data                              #
#                                                                              #
#-##############################################################################

#
# modules
#

use strict;
use warnings qw(FATAL all);
use FindBin qw();
use lib "$FindBin::RealBin/../lib";
use Getopt::Long qw(GetOptions);
use MBSD qw(sort_by_id);
use MBSD::Utils qw();
use No::Worries::Die qw(handler dief);
use No::Worries::Log qw(log_debug log_filter);
use Pod::Usage qw(pod2usage);

#
# global variables
#

our(%Option);

#
# initialization
#

sub init () {
    $| = 1;
    $Option{debug} = 0;
    Getopt::Long::Configure(qw(posix_default no_ignore_case));
    GetOptions(\%Option, MBSD::options()) or pod2usage(2);
    pod2usage(1) if $Option{help};
    pod2usage(exitstatus => 0, verbose => 2) if $Option{manual};
    if ($Option{output}) {
        pod2usage(2) unless @ARGV == 0;
    } else {
        pod2usage(2) unless @ARGV == 1;
        $Option{output} = $ARGV[0];
    }
    log_filter("debug") if $Option{debug};
    log_debug("output directory: %s", $Option{output});
    dief("non existing directory given: %s", $Option{output})
        unless -d $Option{output};
    MBSD::init(%Option);
}

#
# dump all (meaningful) tables in the given directory
#

sub doit ($) {
    my($path) = @_;
    my($separator, $data);

    $separator = ("#" x 76) . "\n";
    foreach my $type (qw(application broker client cluster contact
                         group host secgroup service subcluster user)) {
        $data = $separator;
        foreach my $object (sort_by_id(MBSD::list($type))) {
            $data .= $object->string() . "\n" . $separator;
        }
        local $Option{output} = "$path/$type.dump";
        MBSD::Utils::output($data, %Option);
    }
}

#
# just do it
#

init();
doit($Option{output});

__END__

=head1 NAME

mbsd2dump - generate dump files from MBSD data

=head1 SYNOPSIS

B<mbsd2dump> [I<OPTIONS>] [I<DIRECTORY>]

=head1 DESCRIPTION

This program connects to the MBSD REST API and generates JSON dump files of
all the tables, one file per table. The files will be stored in the given
directory (either given via the B<--output> option or as an argument), which
B<must exist>.

=head1 OPTIONS

=over

=item B<--cookies> I<PATH>

use the SSO cookies from this file for the MBSD authentication
(default: "mbsd-I<instance>-cookies")

=item B<--debug>, B<-d>

show debugging information

=item B<--diff>, B<-D>

show the differences between what is (or would be) saved and the actual file
contents (this is the default when B<--noaction> is used)

=item B<--help>, B<-h>, B<-?>

show some help

=item B<--instance> I<NAME>

use the given MBSD instance (default: "pro")

=item B<--manual>, B<-m>

show this manual

=item B<--noaction>, B<-n>

do not save the output but print the differences instead

=item B<--output>, B<-o> I<PATH>

save the output inside this directory

=back

=head1 AUTHOR

Lionel Cons L<http://cern.ch/lionel.cons>

Copyright (C) CERN 2012-2017
