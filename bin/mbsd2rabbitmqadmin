#!/usr/bin/perl
#+##############################################################################
#                                                                              #
# File: mbsd2rabbitmqadmin                                                     #
#                                                                              #
# Description: generate rabbitmqadmin configuration file from MBSD data        #
#                                                                              #
#-##############################################################################

#
# modules
#

use strict;
use warnings qw(FATAL all);
use FindBin qw();
use lib "$FindBin::RealBin/../lib";
use Getopt::Long qw(GetOptions);
use MBSD qw(sort_by_name);
use MBSD::Utils qw();
use No::Worries::Die qw(handler dief);
use No::Worries::Log qw(log_debug log_filter);
use Pod::Usage qw(pod2usage);

#
# global variables
#

our(%Option, $Output);

#
# initialization
#

sub init () {
    $| = 1;
    $Option{debug} = 0;
    $Option{user} = "system";
    Getopt::Long::Configure(qw(posix_default no_ignore_case));
    GetOptions(\%Option, MBSD::options(),
        "user|u=s",
    ) or pod2usage(2);
    pod2usage(1) if $Option{help};
    pod2usage(exitstatus => 0, verbose => 2) if $Option{manual};
    pod2usage(2) if @ARGV;
    log_filter("debug") if $Option{debug};
    dief("unsupported user: %s", $Option{user})
        unless $Option{user} =~ /^(system|monitor)$/;
    MBSD::init(%Option);
    $Output = MBSD::Utils::header(
        "rabbitmqadmin configuration file (with '$Option{user}' user)"
    ) . "\n";
}

#
# handle a rabbitmqadmin.conf entry
#

sub handle_entry ($$$$) {
    my($host, $broker, $service, $user) = @_;

    $Output .= sprintf("[%s.%s.msg.cern.ch]\n", $broker->name(), $host->name());
    $Output .= sprintf("hostname = %s\n", $host->fullname());
    $Output .= sprintf("port = %d\n", $service->port());
    $Output .= sprintf("declare_vhost = %s\n", $broker->name());
    $Output .= sprintf("username = %s\n", $user->name());
    $Output .= sprintf("password = %s\n", $user->password());
    $Output .= "\n";
}

#
# handle a broker
#

sub handle_broker ($$) {
    my($host, $broker) = @_;
    my($service, $user);

    log_debug(" handling broker %s (%d)", $broker->name(), $broker->id());
    foreach my $object (MBSD::list("service")) {
        next unless $object->broker() eq $broker;
        next unless $object->type() eq "mgmt";
        next unless $object->protocol() eq "tcp";
        next if $object->ssl();
        dief("duplicate mgmt service in broker %d", $broker->id()) if $service;
        $service = $object;
    }
    dief("missing mgmt service in broker %d", $broker->id()) unless $service;
    foreach my $object (MBSD::list("user")) {
        next unless $object->broker(); # broker is optional
        next unless $object->broker() eq $broker;
        next unless $object->name() eq $Option{user};
        next if $object->dn(); # ignore certificates
        dief("duplicate %s user in broker %d", $Option{user}, $broker->id())
            if $user;
        $user = $object;
    }
    dief("missing %s user in broker %d", $Option{user}, $broker->id())
        unless $user;
    handle_entry($host, $broker, $service, $user);
}

#
# handle a host
#

sub handle_host ($) {
    my($host) = @_;

    log_debug("handling host %s (%d)", $host->name(), $host->id());
    foreach my $broker (sort_by_name(MBSD::list("broker"))) {
        next unless $broker->host() eq $host;
        next unless MBSD::Utils::software_flavor($broker) eq "RabbitMQ";
        handle_broker($host, $broker);
    }
}

#
# just do it
#

init();
foreach my $host (sort_by_name(MBSD::list("host"))) {
    handle_host($host);
}
MBSD::Utils::output($Output, %Option);

__END__

=head1 NAME

mbsd2rabbitmqadmin - generate rabbitmqadmin configuration file from MBSD data

=head1 SYNOPSIS

B<mbsd2rabbitmqadmin> [I<OPTIONS>]

=head1 DESCRIPTION

This program connects to the MBSD REST API and generates a configuration
file suitable for C<rabbitmqadmin>. The output is sent to STDOUT unless
the B<--output> option is used.

=head1 OPTIONS

=over

=item B<--cookies> I<PATH>

use the SSO cookies from this file for the MBSD authentication
(default: "mbsd-I<instance>-cookies")

=item B<--debug>, B<-d>

show debugging information

=item B<--diff>, B<-D>

show the differences between what is (or would be) saved and the actual file
contents (this is the default when B<--noaction> is used)

=item B<--help>, B<-h>, B<-?>

show some help

=item B<--instance> I<NAME>

use the given MBSD instance (default: "pro")

=item B<--manual>, B<-m>

show this manual

=item B<--noaction>, B<-n>

when saving the output in a file (B<--output> option), do not save it but
print the differences instead

=item B<--output>, B<-o> I<PATH>

save the output in this file

=item B<--user>, B<-u> I<NAME>

use this user name for the RabbitMQ authentication
(default: C<system>)

=back

=head1 AUTHOR

Lionel Cons L<http://cern.ch/lionel.cons>

Copyright (C) CERN 2012-2017
