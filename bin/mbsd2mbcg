#!/usr/bin/perl
#+##############################################################################
#                                                                              #
# File: mbsd2mbcg                                                              #
#                                                                              #
# Description: generate MBCG configuration files from MBSD data                #
#                                                                              #
#-##############################################################################

#
# modules
#

use strict;
use warnings qw(FATAL all);
use FindBin qw();
use lib "$FindBin::RealBin/../lib";
use Getopt::Long qw(GetOptions);
use MBSD qw(sort_by_name);
use MBSD::Utils qw(cfg indent wrap header);
use No::Worries qw($ProgramName);
use No::Worries::Die qw(handler dief);
use No::Worries::File qw(file_write);
use No::Worries::Log qw(log_debug log_filter);
use Pod::Usage qw(pod2usage);

#
# global variables
#

our(%Option);

#
# initialization
#

sub init () {
    $| = 1;
    $Option{debug} = 0;
    Getopt::Long::Configure(qw(posix_default no_ignore_case));
    GetOptions(\%Option, MBSD::options(),
        "include=s\@",
        "match=s",
    ) or pod2usage(2);
    pod2usage(1) if $Option{help};
    pod2usage(exitstatus => 0, verbose => 2) if $Option{manual};
    pod2usage(2) if @ARGV;
    log_filter("debug") if $Option{debug};
    MBSD::init(%Option);
}

#
# return the group name as known externally
#

sub group_name ($) {
    my($name) = @_;

    $name =~ s/-/_/g;
    return($name);
}

#
# return the list of relevant users in a group
#

sub group_users ($$) {
    my($group, $broker) = @_;
    my(@list, %seen);

    foreach my $user ($group->allusers()) {
        next if $user->broker() and $user->broker() ne $broker;
        push(@list, $user);
        $seen{$user->name()}++;
    }
    return(grep($seen{$_->name()} == 1 || $_->broker(), @list));
}

#
# check if broker and/or subcluster match
#

sub bsc_match ($$) {
    my($object, $info) = @_;

    return($object->broker() eq $info->{broker} ? 1 : undef)
        if $object->broker();
    return($object->subcluster() eq $info->{subcluster} ? 1 : undef)
        if $object->subcluster();
    return(0);
}

#
# return the list of clients assigned to the given broker
#

sub broker_clients ($) {
    my($info) = @_;
    my($assigned, @clients);

    foreach my $client (MBSD::list("client")) {
        $assigned = 0;
        foreach my $application ($client->applications()) {
            $assigned++ if bsc_match($application, $info);
            last if $assigned;
        }
        push(@clients, $client) if $assigned;
    }
    return(@clients);
}

#
# format a user (MBCG broker user)
#

sub format_user ($@) {
    my($user, @groups) = @_;
    my(@config);

    if ($user->dn()) {
        push(@config, cfg("dn", $user->dn()));
    } elsif ($user->password()) {
        push(@config, cfg("password", $user->password()));
    } else {
        dief("unexpected user: %s", $user->string());
    }
    foreach my $name (sort(@groups)) {
        push(@config, cfg("group", $name));
    }
    return(wrap($user->name(), @config));
}

#
# generate the <host> section
#

sub generate_global ($) {
    my($info) = @_;
    my($fullname, $name, $domain);

    $fullname = $info->{host}->fullname();
    if ($fullname =~ /^([a-z0-9]+)((\.[a-z]+)+)$/) {
        ($name, $domain) = ($1, substr($2, 1));
    } else {
        dief("unexpected host: %s", $fullname);
    }
    return(wrap("host", cfg("name", $name), cfg("domain", $domain)), "");
}

#
# generate per-broker defaults
#

sub generate_defaults ($) {
    my($info) = @_;
    my($name, @config);

    $name = $info->{broker}->name();
    push(@config, wrap("broker",
        cfg("name", $name),
        cfg("basedir", "/var/mb/$name"),
        cfg("user", "mb-$name"),
    ));
    return(@config, "");
}

#
# generate the <java> section
#

sub generate_java ($) {
    my($info) = @_;
    my(@config);

    push(@config, cfg("command", $info->{extra}{java_command}))
        if $info->{extra}{java_command};
    push(@config, map(cfg("additional", $_), @{ $info->{extra}{java_options} }))
        if $info->{extra}{java_options};
    return() unless @config;
    return(wrap("java", @config), "");
}

#
# generate the software information
#

sub generate_software ($) {
    my($info) = @_;
    my(@config, $homedir);

    $homedir = sprintf("/usr/share/%s-%s",
        lc($info->{swflavor}), $info->{broker}->software_version());
    push(@config, wrap("broker",
                       cfg("type", $info->{swflavor}),
                       cfg("version", $info->{swmajor}),
                       cfg("homedir", $homedir),
    ));
    return(@config, "");
}

#
# generate the broker <quota> section
#

sub generate_quota ($) {
    my($info) = @_;
    my(%quota, @config);

    foreach my $name (qw(disk memory)) {
        $quota{$name} = $info->{extra}{"${name}_size"}
            if $info->{extra}{"${name}_size"};
    }
    foreach my $name (qw(disk_data disk_log memory_heap)) {
        $quota{$name} = $info->{extra}{$name}
            if $info->{extra}{$name};
    }
    return() unless keys(%quota);
    @config = map(cfg($_, $quota{$_}), sort(keys(%quota)));
    return(wrap("quota", @config), "");
}

#
# generate the high-level MBCG options
#

sub generate_mbcg ($) {
    my($info) = @_;
    my($type, $extra, @config);

    return() unless $info->{extra}{mbcg};
    $type = lc($info->{swflavor});
    $extra = $info->{extra}{mbcg}{$type};
    return() unless $extra;
    foreach my $key (sort(keys(%{ $extra }))) {
        push(@config, cfg($key, $extra->{$key}));
    }
    return() unless @config;
    return(wrap($type, @config), "");
}

#
# generate the <connector> section
#

sub generate_connector ($) {
    my($info) = @_;
    my(@includes, @config, $type, $name, $extra, @options);

    foreach my $service (sort_by_name(MBSD::list("service"))) {
        next unless $service->broker() eq $info->{broker};
        ($type, $name) = ($service->type(), $service->name());
        next if $type eq "jmx"; # handled elsewhere...
        if ($type eq "console") {
            next unless $info->{swflavor} eq "ActiveMQ";
        }
        @options = ();
        $extra = $service->extra();
        if ($extra) {
            if ($type eq "jmx4perl" and
                $extra->{prefix}) {
                @options = wrap("option", cfg("prefix", $extra->{prefix}));
            } elsif ($extra->{mbcg} and
                     $extra->{mbcg}{connector_options}) {
                $extra = $extra->{mbcg}{connector_options};
                foreach my $option (sort(keys(%{ $extra }))) {
                    push(@options, cfg($option, $extra->{$option}));
                }
                @options = wrap("option", @options) if @options;
            }
        }
        push(@includes, sprintf("<<include %s/connector-%s.cfg>>",
                                lc($info->{swflavor}), $name));
        push(@config, wrap($name, cfg("port", $service->port()), @options));
    }
    return(@includes, wrap("connector", @config), "");
}

#
# generate the <jmxremote> section
#

sub generate_jmxremote ($) {
    my($info) = @_;
    my($jmx, @users, @config, $role, $pass);

    # <jmxremote> is only for ActiveMQ...
    return() unless $info->{swflavor} eq "ActiveMQ";
    # ... and only if there is a corresponding service
    foreach my $service (MBSD::list("service")) {
        next unless $service->broker() eq $info->{broker};
        next unless $service->type() eq "jmx";
        dief("duplicate jmx service for broker %s", $info->{broker}->string())
            if $jmx;
        $jmx = $service;
    }
    return() unless $jmx;
    # JMX users come from the security groups named "jmx"
    foreach my $secgroup (MBSD::list("secgroup")) {
        next unless $secgroup->name() eq "jmx";
        next unless defined(bsc_match($secgroup, $info));
        foreach my $user (group_users($secgroup->group(), $info->{broker})) {
            next unless $user->password();
            push(@users, $user);
        }
    }
    # ... and only if there are corresponding users
    return() unless @users;
    foreach my $user (sort_by_name(@users)) {
        # FIXME: how to differentiate between readonly/readwrite access?
        #        tag readwrite accounts with "administrator" secgroup?
        #        current solution: broker-specific account => readwrite
        $role = $user->broker() ? "readwrite" : "readonly";
        $pass = $role eq "readwrite" ? $user->password() : "watcher";
        push(@config, wrap($user->name(),
                           cfg("password", $pass),
                           cfg("role", $role)));
    }
    return(wrap("jmxremote",
                "rmiregistry = " . $jmx->port(),
                wrap("user", @config)), "");
}

#
# generate the broker <user> section
#

sub generate_users ($) {
    my($info) = @_;
    my($group, $group_name, @users, %user_group, @config, %include);

    # 1: go through the matching applications/clients
    foreach my $client (broker_clients($info)) {
        $group = $client->group();
        $group_name = group_name($group->name());
        foreach my $user (group_users($group, $info->{broker})) {
            push(@users, $user) unless $user_group{$user};
            $user_group{$user}{$group_name}++;
        }
    }
    # 2: go through the matching security groups
    foreach my $secgroup (MBSD::list("secgroup")) {
        next unless defined(bsc_match($secgroup, $info));
        $group = $secgroup->group();
        if ($secgroup->name() eq "administrator") {
            # now handled via the special almighty group (globally only)
            $group_name = "almighty";
            next unless $secgroup->subcluster();
        } elsif ($secgroup->name() =~ /^(jetty|monitor)$/) {
            # group membership not needed
            $group_name = undef;
        } elsif ($secgroup->name() eq "jmx") {
            # handled in generate_jmxremote()
            next;
        } else {
            dief("unexpected secgroup: %s", $secgroup->string());
        }
        foreach my $user (group_users($group, $info->{broker})) {
            push(@users, $user) unless $user_group{$user};
            $user_group{$user}{$group_name}++ if $group_name;
        }
    }
    # 3: handle the special almighty group
    ($group) = grep($_->name() eq "special-almighty", MBSD::list("group"));
    if ($group) {
        foreach my $user (group_users($group, $info->{broker})) {
            push(@users, $user) unless $user_group{$user};
            $user_group{$user}{"almighty"}++;
        }
    }
    # 4: format what we have seen
    return() unless @users;
    foreach my $user (sort_by_name(@users)) {
        push(@config, format_user($user, keys(%{ $user_group{$user} })));
    }
    return(wrap("user", @config), "");
}

#
# generate the include statements for the externally managed users/groups
#

sub generate_user_includes ($) {
    my($info) = @_;
    my($group, %include, @config);

    foreach my $client (broker_clients($info)) {
        $group = $client->group();
        foreach my $subgroup ($group->groups()) {
            if ($subgroup->name() eq "cern-dns") {
                $include{"cern-dns.inc"}++;
            }
        }
    }
    return() unless keys(%include);
    foreach my $name (sort(keys(%include))) {
        push(@config, "<<include $name>>");
    }
    return(@config, "");
}

#
# generate the security related sections (ActiveMQ)
#

sub authorizationEntry (%) {
    my(%attr) = @_;
    my($xml);

    $xml = "<authorizationEntry";
    foreach my $name (qw(topic queue read write admin)) {
        $xml .= " $name=\"$attr{$name}\"" if $attr{$name};
    }
    $xml .= "/>";
    return($xml);
}

sub client_groups ($) {
    my($client) = @_;
    my($group, @groups);

    # first: the group directly attached to the client
    $group = group_name($client->group()->name());
    push(@groups, $group);
    # second: its special sub-groups
    $group = $client->group();
    foreach my $subgroup ($group->groups()) {
        if ($subgroup->name() eq "cern-dns") {
            # the group members come from Puppet
            push(@groups, group_name($subgroup->name()));
        }
    }
    return(@groups);
}

sub cltdst_activemq ($) {
    my($client) = @_;
    my($destination);

    $destination = $client->destination();
    $destination =~ s/\{name\}/*/g;
    $destination =~ s/\{path\}/&gt;/g;
    return($destination);
}

sub gensec_activemq ($) {  ## no critic 'ProhibitExcessComplexity'
    my($info) = @_;
    my($type, $dest, $tag, %vqueue, %seen, @config, %declared);
    my(%attr, @list);

    # special handling for the virtual queues
    foreach my $client (broker_clients($info)) {
        $type = $client->destination_type();
        $tag = $client->consumer_tag();
        next unless $type eq "topic" and $tag;
        $dest = cltdst_activemq($client);
        if ($tag eq "{name}") {
            $vqueue{$dest}{$tag} = "Consumer.*.$dest";
        } elsif ($tag =~ /^[a-z0-9\_\-]+$/i) {
            $vqueue{$dest}{$tag} = "Consumer.$tag.$dest";
            $declared{queue}{ $vqueue{$dest}{$tag} }++;
        } else {
            dief("unexpected consumer tag: %s", $client->string());
        }
    }
    # check all clients
    foreach my $client (broker_clients($info)) {
        $type = $client->destination_type();
        $dest = cltdst_activemq($client);
        $tag = $client->consumer_tag();
        if ($tag) {
            $type = "queue";
            $dest = $vqueue{$dest}{$tag};
        }
        unless ($dest =~ /(\*|\&gt\;)/) {
            $declared{$type}{$dest}++;
        }
        if ($client->type() eq "consumer") {
            @list = qw(admin read);
        } else {
            @list = qw(admin write);
        }
        foreach my $group (client_groups($client)) {
            foreach my $priv (@list) {
                $seen{$type}{$dest}{$priv}{$group}++;
            }
        }
        next if $client->type() eq "consumer";
        next unless $type eq "topic" and $vqueue{$dest};
        foreach my $name (keys(%{ $vqueue{$dest} })) {
            foreach my $group (client_groups($client)) {
                foreach my $priv (@list) {
                    $seen{queue}{$vqueue{$dest}{$name}}{$priv}{$group}++;
                }
            }
        }
    }
    @config = ();
    # special: almighty
    %attr = map(($_ => "almighty"), qw(read write admin));
    push(@config,
         "<!-- almighty can do anything -->",
         authorizationEntry(queue => "&gt;", %attr),
         authorizationEntry(topic => "&gt;", %attr));
    # normal
    foreach my $type (sort(keys(%seen))) {
        push(@config, "<!-- $type authorization entries -->");
        foreach my $pattern (sort(keys(%{ $seen{$type} }))) {
            %attr = ();
            foreach my $priv (keys(%{ $seen{$type}{$pattern} })) {
                @list = ();
                foreach my $group (keys(%{ $seen{$type}{$pattern}{$priv} })) {
                    if ($group eq "special_anybody") {
                        @list = ("*");
                        last;
                    } elsif ($group eq "special_nobody") {
                        @list = ();
                        last;
                    } else {
                        push(@list, $group);
                    }
                }
                next unless @list;
                $attr{$priv} = join(",", sort(@list));
            }
            next unless keys(%attr);
            $attr{$type} = $pattern;
            push(@config, authorizationEntry(%attr));
        }
    }
    # record the authorizationPlugin as inline XML
    push(@{ $info->{inline} },
         [ "authorizationEntries", map("  $_", @config) ]);
    # record the destinations to be created as inline XML
    foreach my $type (qw(queue topic)) {
        next unless $declared{$type};
        @list = keys(%{ $declared{$type} });
        $info->{declared}{$type} = [ sort(@list) ] if @list;
    }
    # that's all folks!
    return();
}

#
# generate the security related sections
#

sub generate_security ($) {
    my($info) = @_;

    if ($info->{swflavor} eq "ActiveMQ") {
        return(gensec_activemq($info));
    } else {
        dief("unexpected broker software flavor: %s", $info->{swflavor});
    }
}

#
# generate the declared destinations
#

sub generate_declared ($) {
    my($info) = @_;

    return() unless $info->{declared};
    return(wrap(lc($info->{swflavor}),
        map(cfg("declaredQueue", $_), @{ $info->{declared}{queue} }),
        map(cfg("declaredTopic", $_), @{ $info->{declared}{topic} }),
    ), "");
}

#
# generate inline XML
#

sub generate_inline ($) {
    my($info) = @_;
    my(@config, $name, @lines, $counter);

    return() unless $info->{inline};
    foreach my $inline (@{ $info->{inline} }) {
        ($name, @lines) = @{ $inline };
        $counter++;
        push(@config, "$name <<EOT$counter", @lines, "EOT$counter");
    }
    return(wrap(lc($info->{swflavor}), @config), "");
}

#
# handle a broker
#

sub handle_broker (%) {
    my(%info) = @_;
    my(@config, $output, %map);

    $info{host} = $info{broker}->host();
    $info{id} = MBSD::Utils::broker_uid($info{broker});
    if ($Option{match} and not $info{id} =~ /$Option{match}/) {
        log_debug("  skipping broker %s (%d)", $info{id}, $info{broker}->id());
        return;
    } else {
        log_debug("  handling broker %s (%d)", $info{id}, $info{broker}->id());
    }
    $info{extra} = $info{broker}->extra();
    $info{swflavor} = MBSD::Utils::software_flavor($info{broker});
    $info{swmajor} = MBSD::Utils::software_major($info{broker});
    @config = (header("MBCG configuration file for $info{id}"), "");
    push(@config, generate_global(\%info));
    push(@config, generate_defaults(\%info));
    push(@config, generate_java(\%info));
    push(@config, generate_software(\%info));
    push(@config, generate_quota(\%info));
    push(@config, generate_mbcg(\%info));
    push(@config, generate_connector(\%info));
    push(@config, generate_jmxremote(\%info));
    push(@config, generate_users(\%info));
    push(@config, generate_user_includes(\%info));
    push(@config, generate_security(\%info));
    push(@config, generate_declared(\%info));
    push(@config, generate_inline(\%info));
    if ($Option{include}) {
        foreach my $name (@{ $Option{include} }) {
            push(@config, "<<include $name>>");
        }
        push(@config, "");
    }
    $output = $Option{output};
    %map = (
        "broker"  => $info{broker}->name(),
        "cluster" => $info{cluster}->name(),
        "host"    => $info{host}->fullname(),
    );
    foreach my $name (keys(%map)) {
        $output =~ s/\{$name\}/$map{$name}/g;
    }
    local $Option{output} = $output;
    MBSD::Utils::output(\@config, %Option);
}

#
# handle a subcluster
#

sub handle_subcluster (%) {
    my(%info) = @_;
    my(@brokers);

    log_debug(" handling subcluster %s (%d)",
              $info{subcluster}->name(), $info{subcluster}->id());
    foreach my $broker (sort_by_name(MBSD::list("broker"))) {
        next unless $broker->subcluster() eq $info{subcluster};
        push(@brokers, $broker);
    }
    log_debug("  brokers: %s",
              join(", ", map(MBSD::Utils::broker_uid($_), @brokers)))
        if $Option{debug};
    foreach my $broker (@brokers) {
        handle_broker(%info, broker => $broker, brokers => \@brokers);
    }
}

#
# handle a cluster
#

sub handle_cluster (%) {
    my(%info) = @_;

    log_debug("handling cluster %s (%d)",
              $info{cluster}->name(), $info{cluster}->id());
    foreach my $subcluster (sort_by_name(MBSD::list("subcluster"))) {
        next unless $subcluster->cluster() eq $info{cluster};
        handle_subcluster(%info, subcluster => $subcluster);
    }
}

#
# just do it
#

init();
log_debug("warming up...");
foreach my $type (qw(application group host user)) {
    MBSD::list($type);
}
foreach my $cluster (sort_by_name(MBSD::list("cluster"))) {
    handle_cluster(cluster => $cluster);
}

__END__

=head1 NAME

mbsd2mbcg - generate MBCG configuration files from MBSD data

=head1 SYNOPSIS

B<mbsd2mbcg> [I<OPTIONS>]

=head1 DESCRIPTION

This program connects to the MBSD REST API and generates MBCG configuration
files.

=head1 OPTIONS

=over

=item B<--cookies> I<PATH>

use the SSO cookies from this file for the MBSD authentication
(default: "mbsd-I<instance>-cookies")

=item B<--debug>, B<-d>

show debugging information

=item B<--diff>, B<-D>

show the differences between what is (or would be) saved and the actual file
contents (this is the default when B<--noaction> is used)

=item B<--help>, B<-h>, B<-?>

show some help

=item B<--include> I<STRING>

add to each generated configuration file an Apache-style include directive;
this option can be given multiple times

=item B<--instance> I<NAME>

use the given MBSD instance (default: "pro")

=item B<--manual>, B<-m>

show this manual

=item B<--match> I<REGEXP>

only save the configuration files of the brokers matching the given regular
expression (of the form I<broker-name>@I<host-name>)

=item B<--noaction>, B<-n>

when saving the output in a file (B<--output> option), do not save it but
print the differences instead

=item B<--output>, B<-o> I<STRING>

save the generated files at the given path (which can contain macros to be
expanded); the default is to print on STDOUT

=back

=head1 AUTHOR

Lionel Cons L<http://cern.ch/lionel.cons>

Copyright (C) CERN 2013-2018
