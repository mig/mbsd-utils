#+##############################################################################
#                                                                              #
# File: MBSD.pm                                                                #
#                                                                              #
# Description: Messaging Broker Service Definition                             #
#                                                                              #
#-##############################################################################

package MBSD;

#
# modules
#

use strict;
use warnings qw(FATAL all);
use HTTP::Cookies qw();
use JSON qw(from_json);
use LWP::UserAgent qw();
use No::Worries::Die qw(dief);
use No::Worries::Export qw(export_control);
use No::Worries::Log qw(log_debug);

#
# global variables
#

our(%Cache, $URL, $APIV1, $Debug, $UserAgent, $JSON, $AUTOLOAD);

#+##############################################################################
#                                                                              #
# private helpers                                                              #
#                                                                              #
#-##############################################################################

#
# get an object using the REST API
#

sub _get ($) {
    my($what) = @_;
    my($url, $req, $res, $contents, $data);

    $url = sprintf("%s%s/%s/?format=json", $URL, $APIV1, $what);
    $url .= "&limit=9999" unless $what =~ /\//;
    log_debug("GET(%s)", $url) if $Debug;
    $req = HTTP::Request->new(GET => $url);
    $res = $UserAgent->request($req);
    dief("GET(%s) failed: %s", $url, $res->status_line())
        unless $res->is_success();
    $contents = $res->content();
    log_debug("GET(%s) returned %d bytes", $url, length($contents)) if $Debug;
    eval { $data = $JSON->decode($contents) };
    dief("GET(%s) invalid: %s", $url, $@) if $@;
    return($data);
}

#
# return the list of all users in the given group (handling group inclusion)
#

sub _group2users ($);
sub _group2users ($) {
    my($group) = @_;
    my(%seen, @users);

    unless ($group->{_allusers}) {
        @users = ($group->users(), map(_group2users($_), $group->groups()));
        foreach my $user (@users) {
            $seen{$user} ||= $user;
        }
        $group->{_allusers} = [ values(%seen) ];
    }
    return(@{ $group->{_allusers} });
}

#
# analyze user <-> group relationships
#

sub _analyze () {
    my(%seen);

    # warm-up
    list("user");
    # group -> user
    foreach my $group (list("group")) {
        foreach my $user (_group2users($group)) {
            $seen{$user}{$group} ||= $group;
        }
    }
    # user -> group
    foreach my $user (list("user")) {
        $user->{_allgroups} = [ $seen{$user} ? values(%{ $seen{$user} }) : () ];
    }
}

#
# return the list of all users in the given broker (local users only)
#

sub _broker2users ($) {
    my($broker) = @_;
    my(@users);

    # warm-up
    list("broker");
    # user -> broker
    foreach my $user (list("user")) {
        next unless $user->broker() and $user->broker() eq $broker;
        push(@users, $user);
    }
    return(@users);
}

#
# return the list of all users in the given broker (local users plus client users)
#

sub _broker2allusers ($) {
    my($broker) = @_;
    my($subcluster, @users, %group, $match);

    $subcluster = $broker->subcluster();
    # warm-up
    list("application");
    list("broker");
    list("subcluster");
    # client -> group
    foreach my $client (list("client")) {
        $match = 0;
        foreach my $application ($client->applications()) {
            $match++ if $application->broker()
                and $application->broker() eq $broker;
            $match++ if $application->subcluster()
                and $application->subcluster() eq $subcluster;
            last if $match;
        }
        $group{$client->group()}++ if $match;
    }
    # user -> broker | group
    foreach my $user (list("user")) {
        if ($user->broker()) {
            next unless $user->broker() eq $broker;
        } else {
            next unless grep($group{$_}, $user->allgroups());
        }
        push(@users, $user);
    }
    return(@users);
}

#
# normal getter
#

sub _getter_normal ($$) {
    my($self, $name) = @_;
    my($value);

    $value = $self->{$name};
    dief("unexpected attribute (%s) in object: %s", $name, $self->string())
        unless ref($value) eq "" or JSON::is_bool($value);
    if ($name eq "extra") {
        # JSON
        $value = $value ? from_json($value) : {};
    } elsif ($value and $value =~ /^\Q${APIV1}\E\/(\w+)\/(\d+)\/$/) {
        # reference
        $value = fetch($1, $2);
    }
    return($value);
}

#
# relational getter (n-to-n relationships)
#

sub _getter_relational ($$) {
    my($self, $name) = @_;
    my(@list);

    dief("unexpected attribute (%s) in object: %s", $name, $self->string())
        unless ref($self->{$name}) eq "ARRAY";
    foreach my $uri (@{ $self->{$name} }) {
        if ($uri =~ /^\Q${APIV1}\E\/(\w+)\/(\d+)\/$/) {
            push(@list, fetch($1, $2));
        } else {
            dief("unexpected attribute uri (%s) in object: %s",
                 $uri, $self->string());
        }
    }
    return(@list);
}

#+##############################################################################
#                                                                              #
# sort helpers                                                                 #
#                                                                              #
#-##############################################################################

sub sort_by_id (@)
{ return(sort { $a->{id} <=> $b->{id} } @_); }

sub sort_by_name (@)
{ return(sort { $a->{name} cmp $b->{name} } @_); }
sub sort_by_name_ci (@)
{ return(sort { lc($a->{name}) cmp lc($b->{name}) } @_); }

sub sort_by_title (@)
{ return(sort { $a->{title} cmp $b->{title} } @_); }
sub sort_by_title_ci (@)
{ return(sort { lc($a->{title}) cmp lc($b->{title}) } @_); }

#+##############################################################################
#                                                                              #
# public functions and methods                                                 #
#                                                                              #
#-##############################################################################

#
# return the list of standard options that all mbsd2* scripts should have
#

sub options () {
    return(
        "cookies=s",
        "debug|d+",
        "diff|D!",
        "help|h|?",
        "instance=s",
        "manual|m",
        "noaction|n",
        "output|o=s",
    );
}

#
# initialize the module using the given options (instance and cookies)
#

sub init (%) {
    my(%option) = @_;

    %Cache = ();
    $option{instance} ||= "pro";
    if ($option{instance} eq "pro") {
        $URL = "https://mig-mbsd.cern.ch";
        $option{cookies} ||= "mbsd-pro-cookies";
    } elsif ($option{instance} eq "old") {
        $URL = "https://mb-monitor.cern.ch";
        $option{cookies} ||= "mbsd-old-cookies";
    } elsif ($option{instance} eq "dev") {
        $URL = "https://mb-monitor-dev.cern.ch";
        $option{cookies} ||= "mbsd-dev-cookies";
    } else {
        dief("unsupported MBSD instance: %s", $option{instance});
    }
    $APIV1 = "/mbsd/api/v1";
    $Debug = 1 if $option{debug} > 1;
    $UserAgent = LWP::UserAgent->new(
        # we don't bother checking the host certificate...
        ssl_opts => {
            SSL_verify_mode => 0,
            verify_hostname => 0,
        },
    );
    $UserAgent->cookie_jar(MBSD::Cookies->new(file => $option{cookies}));
    $JSON = JSON->new()->pretty(1)->canonical(1)->relaxed(1);
}

#
# constructor
#

sub new ($@) {
    my($class, @arg) = @_;
    my($self);

    if (@arg == 1 and ref($arg[0]) eq "HASH") {
        $self = $arg[0];
    } elsif (@arg % 2 == 0) {
        $self = { @arg };
    } else {
        dief("unexpected number of arguments for %s->new(): %d",
             __PACKAGE__, scalar(@arg));
    }
    dief("missing option for %s->new(): id", __PACKAGE__)
        unless $self->{id};
    dief("unexpected id for %s->new(): %s", __PACKAGE__, $self->{id})
        unless $self->{id} =~ /^\d+$/;
    dief("missing option for %s->new(): resource_uri", __PACKAGE__)
        unless $self->{resource_uri};
    if ($self->{resource_uri} =~ /^\Q${APIV1}\E\/(\w+)\/($self->{id})\/$/) {
        $self->{_type} = $1;
    } else {
        dief("unexpected resource_uri for %s->new(): %s",
             __PACKAGE__, $self->{resource_uri});
    }
    $self->{_tid} = $self->{_type} . "/" . $self->{id};
    return(bless($self, __PACKAGE__));
}

#
# fetch an object by type and id (using caching)
#

sub fetch ($$) {
    my($type, $id) = @_;
    my($cid);

    $cid = "$type/$id";
    $Cache{$cid} ||= __PACKAGE__->new(_get($cid));
    return($Cache{$cid});
}

#
# get the list of all the objects of the given type (using caching)
#

sub list ($) {
    my($type) = @_;
    my($data, $cid);

    unless ($Cache{$type}) {
        $Cache{$type} = [];
        $data = _get($type);
        foreach my $object (@{ $data->{objects} }) {
            $cid = $type . "/" . $object->{id};
            $Cache{$cid} ||= __PACKAGE__->new($object);
            push(@{ $Cache{$type} }, $Cache{$cid});
        }
    }
    return(@{ $Cache{$type} });
}

#
# return the object's string representation
#

sub string : method {
    my($self) = @_;
    my(%hash);

    %hash = %{ $self };
    delete($hash{_allusers});
    delete($hash{_allgroups});
    foreach my $key (keys(%hash)) {
        next unless $key =~ /_member$/;
        next unless ref($hash{$key}) eq "ARRAY";
        $hash{$key} = [ sort(@{ $hash{$key} }) ];
    }
    return($JSON->encode(\%hash));
}

#
# generic getter
#

sub AUTOLOAD {  ## no critic 'ProhibitAutoloading'
    my($self) = @_;
    my($name);

    $name = $AUTOLOAD;
    $name =~ s/.*://; # strip fully-qualified portion
    return() if $name =~ /^[A-Z]+$/; # Perl internal
    dief("no such function/method: %s(%d)", $AUTOLOAD, scalar(@_))
        unless $self and ref($self) and @_ == 1;
    if ($name !~ /_member$/ and exists($self->{$name})) {
        # normal getter
        return(_getter_normal($self, $name))
    }
    if ($name =~ /^(\w+)s$/ and exists($self->{"${1}_member"})) {
        # n-to-n relationships
        return(_getter_relational($self, "${1}_member"));
    }
    if ($name eq "allusers" and $self->{_type} eq "group") {
        _analyze() unless $self->{_allusers};
        return(@{ $self->{_allusers} });
    }
    if ($name eq "allgroups" and $self->{_type} eq "user") {
        _analyze() unless $self->{_allgroups};
        return(@{ $self->{_allgroups} });
    }
    if ($name eq "users" and $self->{_type} eq "broker") {
        return(_broker2users($self));
    }
    if ($name eq "allusers" and $self->{_type} eq "broker") {
        return(_broker2allusers($self));
    }
    # so far so bad
    dief("unknown attribute (%s) in object: %s", $name, $self->string());
}

#
# export control
#

sub import : method {
    my($pkg, %exported);

    $pkg = shift(@_);
    grep($exported{$_}++, map("sort_by_${_}", qw(id name title)));
    grep($exported{$_}++, map("sort_by_${_}_ci", qw(name title)));
    export_control(scalar(caller()), $pkg, \%exported, @_);
}

1;

#+##############################################################################
#                                                                              #
# working stripped down version of HTTP::Cookies::Netscape (wrt expiration)    #
#                                                                              #
#-##############################################################################

package MBSD::Cookies;

#
# modules
#

use strict;
use warnings;
use No::Worries::Die qw(dief);
use No::Worries::File qw(file_read);

#
# inheritance
#

our @ISA = qw(HTTP::Cookies);

#
# load cookies from a Netscape HTTP cookie file
#

sub load : method {
    my($self, $file) = @_;
    my(@lines, $now, $domain, $bool1, $path, $secure, $expires, $key, $val);

    $file ||= $self->{file} || return(0);
    @lines = split(/\n/, file_read($file));
    $val = shift(@lines);
    dief("invalid Netscape cookie file: %s", $val)
        unless $val =~ /^\#(?: Netscape)? HTTP Cookie File/;
    $now = time();
    foreach my $line (@lines) {
        next if $line =~ /^\s*\#/;
        next if $line =~ /^\s*$/;
        $line =~ tr/\n\r//d;
        ($domain, $bool1, $path, $secure, $expires, $key, $val) =
            split(/\t/, $line);
        $secure = $secure eq "TRUE";
        # 0 is assumed to be never i.e. in 30 days ;-)
        $expires ||= $now + 30 * 24 * 3600;
        $self->set_cookie(undef, $key, $val, $path, $domain, undef, 0,
                          $secure, $expires-$now, 0);
    }
    return(1);
}

1;

__DATA__

=head1 NAME

MBSD - Messaging Broker Service Definition interface

=head1 SYNOPSIS

FIXME

=head1 DESCRIPTION

FIXME

=head1 FUNCTIONS

This module provides the following functions:

=over

=item MBSD::options()

return the list of standard options

=item MBSD::init(OPTIONS)

initialize the module using the given options

=item MBSD::list(TYPE)

return the list of all objects of the given type

=item MBSD::fetch(TYPE, ID)

return the object of the given type and id

=back

Note: all the functions use a shared cache so an object will only be
fetched once from the REST API. To clear the cache, call MBSD::init()
again.

=head1 OBJECTS

An MBSD object is a blessed hash that contains all the attributes
returned by MBSD. In addition, it also contains the following
attributes:

=over

=item _type: the object's type

=item _tid: the object's typed id (i.e. C<_type> and C<id>)

=back

Note: all MBSD objects have a numerical C<id> attribute as well as a
C<resource_uri> attribute set by the REST API.

Each attribute can be accessed via its eponymous method. For instance,
the id() method will return the object's C<id>.

When the attribute being accessed points to another object, the object
pointed to is returned. For instance:

  $subcluster = MBSD::fetch("subcluster", "3");
  $cluster = $subcluster->cluster();
  $name = $cluster->name();

When the attribute being accessed points to a list of objects (via
n-to-n relationships), the list of objects is returned. Also, the
C<_member> suffix present in the object is not needed. For instance:

  $client = MBSD::fetch("client", "1");
  @applications = $client->applications();

MBSD objects are cached so there is only one Perl object per MBSD object.
Therefore, one can easily compare objects as strings:

  next unless $subcluster->cluster() eq $cluster;

The C<extra> attribute contains arbitrary data encoded in JSON. The
extra() method decodes this data so that it can be accessed directly:

  $extra = $broker->extra();
  if ($extra->{foo}) { ... }

The string() method returns the object's contents in JSON format.

Users and groups are a bit special because of recursion (groups can
include groups). Convenient methods are provided to handle recursion.

Here are the relevant additional user methods:

=over

=item groups(): return the list of groups directly including this user

=item allgroups(): return the list of groups including this user recursively

=back

Here are the relevant additional group methods:

=over

=item groups(): return the list of groups directly included by this group

=item users(): return the list of users directly included by this group

=item allusers(): return the list of users recursively included by this group

=back

Here are the relevant additional broker methods:

=over

=item users(): return the list of users directly attached to this broker

=item allusers(): return the list of all users related to this broker

=back

=head1 AUTHOR

Lionel Cons L<http://cern.ch/lionel.cons>

Copyright (C) CERN 2012-2017
